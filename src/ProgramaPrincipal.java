import java.util.Scanner;

public class ProgramaPrincipal {

    public static void main(String[] args) {

        System.out.println("***************************");
        System.out.println("Folha de pagamento");
        System.out.println("***************************\n");
        Scanner sc = new Scanner(System.in);

//        System.out.println("Informe a quantidade de horas trabalhadas: ");
//        int quantidadeHoras = sc.nextInt();
//        System.out.println("Informe a quantidade de horas extras trabalhadas: ");
//        int quantidadeHorasExtras = sc.nextInt();
//        System.out.println("Informe o valor da hora normal: ");
//        double valorHoraNormal = sc.nextDouble();
//        System.out.println("Informe o valor da hora trabalhada: ");
//        double valorHoraExtra = sc.nextDouble();
//        System.out.println("Funcionário possui filhos? ");
//        boolean resposta = sc.nextBoolean();

        Funcionario funcionario = new Funcionario("João", 2);
        FolhaPagamento folhaPagamento = new FolhaPagamento();
        ContratoTrabalho contratoTrabalho = new ContratoTrabalho(funcionario, 14.5, 19.0);

        double resultado = folhaPagamento.calcularSalario(contratoTrabalho);
        System.out.println("Salário do funcionário: R$ " + resultado);

    }

}
