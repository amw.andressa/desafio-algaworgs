public class FolhaPagamento {

    public FolhaPagamento(){}

    private int quantidadeHoras;
    private int quantidadeHoraExtra;
    public FolhaPagamento(int quantidadeHoras, int quantidadeHoraExtra){
        this.quantidadeHoras = quantidadeHoras;
        this.quantidadeHoraExtra = quantidadeHoraExtra;
    }

    public int getQuantidadeHoras() {
        return quantidadeHoras;
    }

    public void setQuantidadeHoras(int quantidadeHoras) {
        this.quantidadeHoras = quantidadeHoras;
    }

    public int getQuantidadeHoraExtra() {
        return quantidadeHoraExtra;
    }

    public void setQuantidadeHoraExtra(int quantidadeHoraExtra) {
        this.quantidadeHoraExtra = quantidadeHoraExtra;
    }

    ContratoTrabalho contratoTrabalho = new ContratoTrabalho();

    public double calcularSalario(ContratoTrabalho contrato){
        double salarioTotal = (contrato.getValorHoraNormal() * contrato.getFuncionario().quantidadeDeFilhos()) +
                (contrato.getValorHoraExtra() * contrato.getFuncionario().quantidadeDeFilhos());

        if(contratoTrabalho.possuiAdicionalDeFilhos()){
            salarioTotal *= 1.1;
        }

        return salarioTotal;
    }

}
