public class Funcionario {

    public String nome;
    public int quantidadeDeFilhos;

    public Funcionario(){}

    public Funcionario(String nome, int quantidadeDeFilhos){
        this.nome = nome;
        this.quantidadeDeFilhos = quantidadeDeFilhos;
    }
    public int quantidadeDeFilhos() {
        return quantidadeDeFilhos;
    }
    public boolean possuiFilhos(){
        return quantidadeDeFilhos > 0;
    }
}
