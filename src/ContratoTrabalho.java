public class ContratoTrabalho {

    Funcionario funcionario = new Funcionario();
    double valorHoraNormal;
    double valorHoraExtra;

    public ContratoTrabalho(){}

    public ContratoTrabalho(Funcionario funcionario, double valorHoraNormal, double valorHoraExtra){
        this.funcionario = funcionario;
        this.valorHoraNormal = valorHoraNormal;
        this.valorHoraExtra = valorHoraExtra;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public ContratoTrabalho(double valorHoraNormal, double valorHoraExtra){
        this.valorHoraNormal = valorHoraNormal;
        this.valorHoraExtra = valorHoraExtra;
    }

    public boolean possuiAdicionalDeFilhos(){
        return funcionario.possuiFilhos();
    }

    public double getValorHoraNormal() {
        return valorHoraNormal;
    }

    public void setValorHoraNormal(double valorHoraNormal) {
        this.valorHoraNormal = valorHoraNormal;
    }

    public double getValorHoraExtra() {
        return valorHoraExtra;
    }

    public void setValorHoraExtra(double valorHoraExtra) {
        this.valorHoraExtra = valorHoraExtra;
    }
}
